#include <iostream> 
#include "animals.h"


using namespace std;


int main() {

	Animal* baseAnimal; //model class
	Animal* baseCat;
	Animal* baseDog;
	Animal* baseTurtle;
	Animal* baseBulbasaur;
	Animal* basePikachu;
	Animal* baseSnorlax;
	Animal* BaseUltimatePokemon; // Ultimate pokemon 

	Animal myAnimal("Animal", 0, 0); //model class
	Cat myCat("Cat", 5, 12, "Scratch"); //one ability, so contructer takes an extra argument
	Dog myDog("Dog", 12, 4, "Bite"); //one ability, so contructer takes an extra argument
	Turtle myTurtle("Turtle", 68, 1, "vanish"); //one ability, so contructer takes an extra argument
	Pokemon bulbasaur("Bulbasaur", 4, 1, "Scratch", "Vanish"); //Pokemon has two abilities, so the constructer takes in an extra argument. 
	Pokemon pikachu("Pikachu", 11, 60, "Thunder shock", "Tail Whip");//Pokemon has two abilities, so the constructer takes in an extra argument.
	Pokemon snorlax("Bulbasaur", 124, 0, "sleep", "eat"); //Pokemon has two abilities, so the constructer takes in an extra argument.
	UltimatePokemon chugga("Chugga", 98, 300, "Eat", "Mold", "Squash"); //Pokemon has three abilities, so the constructer takes in an extra argument.

	baseAnimal = &myAnimal;
	baseCat = &myCat;
	baseDog = &myDog;
	baseTurtle = &myTurtle;
	baseBulbasaur = &bulbasaur;
	basePikachu = &pikachu;
	baseSnorlax = &snorlax;
	BaseUltimatePokemon = &chugga;


	cout << baseAnimal->pullName(); //pulls the name of the animal or pokemon, essentially pulls the name of the instnace.
	baseAnimal->attack(); //Animal Attacks
	baseAnimal->sound(); //Animal makes a sound
	baseAnimal->lunge(0); // lunge function takes in an int
	baseAnimal->lunge(2.5); //lunge function takes in a double
	baseAnimal->lunge(4, 5); //lunge function takes in 2 arguments
	baseAnimal->describe();//Describe the features of the base animal, this is just model. 
	baseAnimal->changeLungeDistance(10);//changes the lunge distance

	cout << baseCat->pullName(); //pulls the name of the animal 
	baseCat->attack(); //Cat Attacks
	baseCat->sound(); //Cat makes a sound
	baseCat->lunge(12); // lunge function takes in an int
	baseCat->lunge(12.5); //lunge function takes in a double
	baseCat->lunge(12, 2); //lunge function takes in 2 arguments
	baseCat->changeLungeDistance(10);//changes the lunge distance
	baseCat->describe();//Describe the features of the cat, 

	cout << baseDog->pullName(); //pulls the name of the animal or pokemon, 
	baseDog->attack(); //Dog Attacks
	baseDog->sound(); //Dog makes a sound
	baseDog->lunge(4); // lunge function takes in an int
	baseDog->lunge(4.5); //lunge function takes in a double
	baseDog->lunge(4, 3); //lunge function takes in 2 arguments
	baseDog->changeLungeDistance(10);//changes the lunge distance
	baseDog->describe();//Describe the features of the base Dog.

	cout << baseTurtle->pullName(); //pulls the name of the animal 
	baseTurtle->attack(); //Turtle Attacks
	baseTurtle->sound(); //Turtle makes a sound
	baseTurtle->lunge(1); // lunge function takes in an int
	baseTurtle->lunge(1.2); //lunge function takes in a double
	baseTurtle->lunge(1, 1); //lunge function takes in 2 arguments
	baseTurtle->changeLungeDistance(1);//changes the lunge distance
	baseTurtle->describe();//Describe the features of the base Turtle. 

	cout << baseBulbasaur->pullName(); //pulls the name of the pokemon 
	baseBulbasaur->attack(); //Bulbasaur Attacks
	baseBulbasaur->sound(); //Bulbasaur makes a sound
	baseBulbasaur->lunge(1); // lunge function takes in an int
	baseBulbasaur->lunge(1.2); //lunge function takes in a double
	baseBulbasaur->lunge(1, 1); //lunge function takes in 2 arguments
	baseBulbasaur->changeLungeDistance(1);//changes the lunge distance
	bulbasaur.abilities();//shows bulbasaur abilities
	baseBulbasaur->describe();//Describe the features of the Pokemon > Bulbasaur.

	cout << basePikachu->pullName(); //pulls the name of the poekomon 
	basePikachu->attack(); //pikachu Attacks
	basePikachu->sound(); //pikachu makes a sound
	basePikachu->lunge(21); // lunge function takes in an int
	basePikachu->lunge(20.5); //lunge function takes in a double
	basePikachu->lunge(1, 1); //lunge function takes in 2 arguments
	basePikachu->changeLungeDistance(1);//changes the lunge distance
	pikachu.abilities();//shows pikachu abilities
	basePikachu->describe();//Describe the features of the Pokemon > pikachu.

	cout << baseSnorlax->pullName(); //pulls the name of the Snorlax 
	baseSnorlax->attack(); //Snorlax Attacks
	baseSnorlax->sound(); //Snorlax makes a sound
	baseSnorlax->lunge(0); // lunge function takes in an int
	baseSnorlax->lunge(0.1); //lunge function takes in a double
	baseSnorlax->lunge(1, 1); //lunge function takes in 2 arguments
	baseSnorlax->changeLungeDistance(1);//changes the lunge distance
	snorlax.abilities();//shows snorlax abilities
	baseSnorlax->describe();//Describe the features of the Pokemon > Snorlax

	cout << BaseUltimatePokemon->pullName(); //pulls the name of the chugga 
	BaseUltimatePokemon->attack(); //chugga Attacks
	BaseUltimatePokemon->sound(); //chugga makes a sound
	BaseUltimatePokemon->lunge(300); // lunge function takes in an int
	BaseUltimatePokemon->lunge(400.3); //lunge function takes in a double
	BaseUltimatePokemon->lunge(450, 2); //lunge function takes in 2 arguments
	BaseUltimatePokemon->changeLungeDistance(450);//changes the lunge distance
	chugga.abilities();//shows chugga's abilities
	BaseUltimatePokemon->describe();//Describe the features of the Pokemon > Snorlax
}
