


using namespace std;

// Compile time polymorphism: This type of polymorphism is achieved by function overloading or operator overloading. 

class Animal {

public:
	Animal(string animalName, int animalAge, int lungeDistance) :  // member initializer list
		name(animalName), age(animalAge), lungeD(lungeDistance) //lungeD means lunge Distance, I can't use lunge because that's a function name. 
	{
		// empty constructor
	}

	virtual string pullName() {
		return name;
	}

	void changeLungeDistance(int x) {
		lungeD = x; //lungeD because what the user puts as an argument. 
	}

	void attack() {
		"Animal Lunges and attacks";
	}
	virtual void sound() { //Animal or pokemon making sound
		cout << "animal making sound\n"; //\n prints a new line
	}
	virtual void lunge(int lunge_distance) { // function overloading,  
		cout << "animal lunges by " << lunge_distance << "ft\n";
	}
	virtual void lunge(double lunge_distance) {

		cout << "animal lunges by " << lunge_distance << "ft\n";
	}
	virtual void lunge(int lunge_distance, int y) { //lunge by a lunge_distance * y.  

		cout << "animal lunges by " << lunge_distance * y << "ft\n";
	}
	virtual void describe() {
		printf("\n----\nAnimal: %s\nAge: %d\nLunge Distance: %d ft\n----\n", name.c_str(), age, lungeD);
	}
protected:
	string name;
	int age;
	int lungeD;
};

/*
the primary benefit of function overloading is  consistency in the naming of methods
functions which logically perform very similar tasks, and differ
slightly in by accepting different parameters. This allows the same
method name to be reused across multiple implementations.
*/


class Cat : virtual public Animal {

public:
	Cat(string animalName, int animalAge, int lungeDistance, string catAttack) : // Added the property catAttack which gives the cat a special attack
		cAttack(catAttack), //cAttack c stand for Cat. 
		Animal(animalName, animalAge, lungeDistance)
	{
		//Empty constructer
	}


	void attack() {
		printf("\n%s lunges and uses it's %s attack\n", name.c_str(), cAttack.c_str()); //"c_str() lets us print strings. 
	}

	void sound() { //cat making sound
		cout << "meow meow\n";
	}
	void lunge(int lungeD) { // function overloading, 
		printf("%s lunges by %d ft\n", name.c_str(), lungeD);
	}
	void lunge(double lungeD) {

		printf("%s lunges by %f ft\n", name.c_str(), lungeD);
	}
	void lunge(int lungeD, int y) { //lunge by a lunge_distance * y.  

		printf("%s lunges by %d ft\n", name.c_str(), lungeD);
	}
	void describe() {
		printf("\n----\nAnimal: %s\nAge: %d\nLunge Distance: %d ft\nSpecial Attakc: %s\n----\n", name.c_str(), age, lungeD, cAttack.c_str());
	}
protected:
	string cAttack;
};

class Dog : virtual public Animal {

public:
	Dog(string animalName, int animalAge, int lungeDistance, string dogAttack) : // Added the property biteAttack which allows the dog to bite someone
		dAttack(dogAttack),
		Animal(animalName, animalAge, lungeDistance)
	{
		//Empty constructer
	}

	string getAttack() {
		return dAttack; //This will return the Dogs special attack. 
	}

	void sound() { //dog making sound
		cout << "Ra Ra Ra\n";
	}


	void attack() {
		printf("%s lunges and uses it's %s attack", name.c_str(), dAttack.c_str());
	}

	void lunge(int lunge_distance) { // function overloading,   
		printf("%s lunges by %d ft\n", name.c_str(), lungeD);
	}
	void lunge(double lunge_distance) {

		printf("%s lunges by %d ft\n", name.c_str(), lungeD);
	}
	void lunge(int lunge_distance, int y) { //lunge by a lunge_distance * y.  

		printf("%s lunges by %d ft\n", name.c_str(), lungeD);
	}
	void describe() {
		printf("\n----\nAnimal: %s\nAge: %d\nLunge Distance: %d ft\nSpecial Attakc: %s\n----\n", name.c_str(), age, lungeD, dAttack.c_str());
	}

protected:
	string dAttack;
};

class Turtle : virtual public Animal { //single inherticance

public:
	Turtle(string animalName, int animalAge, int lungeDistance, string turtleAttack) : // Added the property biteAttack which allows the dog to bite someone
		tAttack(turtleAttack), //turtleAttack
		Animal(animalName, animalAge, lungeDistance)
	{
		//Empty constructer
	}

	string getAttack() {
		return tAttack; //returns attack
	}


	void animal_sound() {
		cout << "\nmuph muph\n";
	}
	void Attack() {
		printf("%s lunges and uses it's %s attack", name.c_str(), tAttack.c_str());
	}
	void lunge(int lunge_distance) { // function overloading, we're using the same function name but with different paramaters.  
						// first func shows lung distance in int, second show lung distance in double and third multiples lunge by a specific amount.  
		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(double lunge_distance) {

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(int lunge_distance, int y) { //lunge by a lunge_distance * y.  

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void describe() {
		printf("\n----\nAnimal: %s\nAge: %d\nLunge Distance: %d cm\nSpecial Attakc: %s\n----\n", name.c_str(), age, lungeD, tAttack.c_str());
	}
protected:
	string tAttack;
};

class Pokemon : public Cat, public Turtle { //inherits from both Cat and Turtle which virtualy inherits class Animal
public:
	Pokemon(string animalName, int animalAge, int lungeDistance, string catAttack, string turtleAttack) : // Added the property biteAttack which allows the dog to bite someone
		 //bulbasaurAttack
		Cat(animalName, animalAge, lungeDistance, catAttack),
		Turtle(animalName, animalAge, lungeDistance, turtleAttack),
		Animal(animalName, animalAge, lungeDistance)
	{
		//Empty constructer
	}
	string getAttack() {
		return cAttack + tAttack; string concatenation;
	}

	void sound() {
		printf("%s making sound\n", name.c_str());
	}

	void Attack() {
		printf("%s lunges and uses it's %s and %s attack", name.c_str(), cAttack.c_str(), tAttack.c_str());
	}
	void lunge(int lunge_distance) { // function overloading, we're using the same function name but with different paramaters.  
						// first func shows lung distance in int, second show lung distance in double and third multiples lunge by a specific amount.  
		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(double lunge_distance) {

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(int lunge_distance, int y) { //lunge by a lunge_distance * y.  

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void pokemonSound() {
		printf("%s is making noise", name.c_str());
	}
	void fuse() { //Fuses the Cat and Turtle to create a bulbasaur Pokémon. 
		printf("HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA! Cat + Turtle fusion\n\n...%s has been formed\n\nbrr...brrr...brrr...Bulbasaur is making noises\n\n", name.c_str()); // Print text to show the fusion of cat and turtle	
	}


	void abilities() {
		printf("Ability 1: %s\nAbility 2: %s", cAttack.c_str(), tAttack.c_str());
	}

	void describe() {
		printf("\n----\nPokmon: %s\nAge: %d\nLunge Distance: %d ft\nSpecial Attakc1: %s\nSpecial Attakc2: %s\n----\n", name.c_str(), age, lungeD, cAttack.c_str(), tAttack.c_str());
	}
};

//class Ultimate_animal : public Cat, public Dog, public Elephant;


class UltimatePokemon : public Cat, public Dog, public Turtle { // Hierarchical Inheritance
public:
	UltimatePokemon(string animalName, int animalAge, int lungeDistance, string catAttack, string dogAttack, string turtleAttack) : // Added the property biteAttack which allows the dog to bite someone
		 //bulbasaurAttack
		Cat(animalName, animalAge, lungeDistance, catAttack),
		Dog(animalName, animalAge, lungeDistance, dogAttack),
		Turtle(animalName, animalAge, lungeDistance, turtleAttack),
		Animal(animalName, animalAge, lungeDistance)
	{
		//Empty constructer
	}
	string getAttack() {
		return cAttack + tAttack + dAttack; //string concatenation
	}

	void sound() {
		printf("%s making sound\n", name.c_str());
	}

	void Attack() { //function to attack
		printf("%s lunges and uses it's %s and %s attack and %s attack", name.c_str(), cAttack.c_str(), dAttack.c_str(), tAttack.c_str());
	}

	void lunge(int lunge_distance) { // function overloading, we're using the same function name but with different paramaters.  
						// first func shows lung distance in int, second show lung distance in double and third multiples lunge by a specific amount.  
		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(double lunge_distance) {

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void lunge(int lunge_distance, int y) { //lunge by a lunge_distance * y.  

		printf("%s lunges by %d cm\n", name.c_str(), lungeD);
	}
	void pokemonSound() {
		printf("%s is making noise", name.c_str());
	}
	void fuse() { //Fuses the Cat and Turtle to create a bulbasaur Pokémon. 
		printf("HAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA! Cat + Turtle + Dog fusion\n\n...%s has been formed\n\nbrr...brrr...brrr...Bulbasaur is making noises\n\n", name.c_str()); // Print text to show the fusion of cat and turtle	
	}


	void abilities() {
		printf("Ability 1: %s\nAbility 2: %s\nAbility 3: %s", cAttack.c_str(), dAttack.c_str(), tAttack.c_str());
	}

	void describe() {
		printf("\n----\nPokmon: %s\nAge: %d\nLunge Distance: %d ft\nSpecial Attakc1: %s\nSpecial Attakc2: %s\nSpecial Attakc2: %s\n----\n", name.c_str(), age, lungeD, cAttack.c_str(), dAttack.c_str(), tAttack.c_str());
	}
};
